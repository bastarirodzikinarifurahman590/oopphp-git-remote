<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "name = $sheep->name <br>";// shaun
echo "legs = $sheep->legs <br>";// 2
echo "cold blooded = $sheep->cold_blooded <br><br>";// false

$sungokong = new Ape("kera sakti");

echo "name = $sungokong->name <br>";// shaun
echo "legs = $sungokong->legs <br>";// 2
echo "cold blooded = $sungokong->cold_blooded <br>";
$sungokong->yell();
echo "<br><br>";
$kodok = new Frog("buduk");

echo "name = $kodok->name <br>";// shaun
echo "legs = $kodok->legs <br>";// 2
echo "cold blooded = $kodok->cold_blooded <br>";
$kodok->jump();
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>